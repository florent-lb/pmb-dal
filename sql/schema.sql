create table account
(
    id        int auto_increment
        primary key,
    bank_name varchar(255) not null,
    iban      varchar(34)  not null,
    id_user   int          null
);

create table hibernate_sequence
(
    next_val bigint null
);

create table transaction_state
(
    id  smallint    not null
        primary key,
    tag varchar(10) null
);

create table user_app
(
    id         int auto_increment
        primary key,
    email      varchar(255) null,
    firstName  varchar(255) null,
    iteration  smallint     null,
    lastName   varchar(255) not null,
    login_user varchar(255) null,
    password   char(32)     not null,
    role_app   varchar(255) null,
    salt       char(24)     not null,
    wallet     double       not null,
    id_account int          not null,
    constraint UK_mk1xvwget6ponrb1elcqe09uv
        unique (email),
    constraint UK_t4rl5gtx4cbkitlgahpo0qwjv
        unique (id_account),
    constraint FK_account_id_user_id_account
        foreign key (id_account) references account (id)
);

alter table account
    add constraint FK_user_id_account_id_user
        foreign key (id_user) references user_app (id);

create table internal_transaction
(
    date_start  datetime(6)    not null,
    date_end    datetime(6)    null,
    description varchar(1000)  null,
    tax         decimal(9, 2)  null,
    value       decimal(10, 2) null,
    id_state    smallint       not null,
    id_sender   int            not null,
    id_receiver int            not null,
    primary key (id_state, id_sender, id_receiver, date_start),
    constraint FK_state_id_internal_transaction_state
        foreign key (id_state) references transaction_state (id),
    constraint FK_user_id_internal_transaction_receiver
        foreign key (id_receiver) references user_app (id),
    constraint FK_user_id_internal_transaction_sender
        foreign key (id_sender) references user_app (id)
);

create index idx_email
    on user_app (email);

create table user_list
(
    id_owner int not null,
    id_user  int not null,
    constraint FK_user_id_user_id_owner
        foreign key (id_owner) references user_app (id),
    constraint FK_user_id_user_id_user
        foreign key (id_user) references user_app (id)
);

create index idx_owner
    on user_list (id_owner);

create table way
(
    id    smallint    not null
        primary key,
    label varchar(25) null
);

create table external_transaction
(
    date_start datetime(6)    not null,
    date_end   datetime(6)    null,
    tax        decimal(9, 2)  null,
    value      decimal(10, 2) null,
    id_user    int            not null,
    id_state   smallint       not null,
    id_way     smallint       not null,
    primary key (date_start, id_state, id_user),
    constraint FK_state_id_external_transaction_state
        foreign key (id_state) references transaction_state (id),
    constraint FK_user_id_external_transaction_id
        foreign key (id_user) references user_app (id),
    constraint FK_way_id_external_transaction_way
        foreign key (id_way) references way (id)
);

