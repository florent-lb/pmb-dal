INSERT INTO pmb.way VALUES (1,'FROM_APP_TO_BANK_ACCOUNT');
INSERT INTO pmb.way VALUES (2,'FROM_BANK_ACCOUNT_TO_APP');

INSERT INTO pmb.transaction_state VALUES (1,'BEGIN');
INSERT INTO pmb.transaction_state VALUES (10,'WAIT');
INSERT INTO pmb.transaction_state VALUES (20,'END');