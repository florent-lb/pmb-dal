# Project OC 6 - Pay My Budy
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_pmb-dal&metric=alert_status)](https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_pmb-dal&metric=bugs)](https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_pmb-dal&metric=coverage)](https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_pmb-dal&metric=ncloc)](https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_pmb-dal&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal)

## The project : Pay My Budy
PMB is an application for people who want exchange money without use classical bank services. 
The application propose to the user to create is account with a virtual wallet, from this wallet he can send money to a friend in his list.
The user can fill his wallet from his bank account and get his money back to his account too.

This repository is the data access layer off this application.

## Useful Links
<a href="https://sonarcloud.io/dashboard?id=florent-lb_pmb-dal">SonarCloud of the project</a><br/>
<a href="https://quarkus.io/">Quarkus</a><br/>
<a href="https://hub.docker.com/_/mysql">Docker Hub - Mysql</a>
## Build
Use Docker (Dockerfile at root of the project) for Mysql server or use your own server with credential in application.properties(or update it too).

Command to prepare Mysql BDD :
At the root of the project
1. Build the image :
<code>docker build . -t pmb-dal:1.0</code>
2. Run the image
<code>docker run pmb-dal:1.0</code>
3. Connect to the BDD : <br/>
    login : dal         <br/>
    password : 2iplnN9Vh55AdeFC
4. Be sure your current schema is pmb <code>USE pmb;</code>
5. Run SQL command in sql/schema.sql
6. Run SQL command in sql/data.sql



<em>If you don't use Docker container</em>, create database before use schema.sql 
Mysql :<br/><code>CREATE DATABASE db_name;<br/>
CREATE USER 'dal' IDENTIFIED BY '2iplnN9Vh55AdeFC';<br/>
GRANT SELECT ON TABLE pmb.* TO 'dal';<br/>
GRANT SELECT, INSERT, TRIGGER ON TABLE pmb.* TO 'dal';</code>

for default login/password : <strong>dal</strong> / <strong>2iplnN9Vh55AdeFC</strong>

For generate coverage report in target/site/jacoco : `clean verify`

SQL DLL is generated rom Hibernate-ORM for test and dev mode.<br/>
1.Use sql/schema.sql for DLL production <br/>
2.Use sql/data.sql for data insert after DLL<br/>

## MPD for db pmb
1. Data Structure
![alt text](sql/MPD.png "MPD")
2. Relational Structure
![alt text](sql/MPD-relational.jpg "MPD")