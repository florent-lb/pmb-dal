package com.pmb.test.integration.rest;

import com.pmb.model.dao.entity.Account;
import com.pmb.model.dao.entity.UserApp;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.*;

import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
@Tag("integration")
public class UserEndPointTest {

    @Test
    @DisplayName("Search known user")
    public void getUser_whenSearchUserByEmail_ShouldReturnOneUser() {

        String emailTest = "toto@toto.com";

        given()
          .when().param("email",emailTest).auth().basic("t.to","haha")
                .get("/user")
          .then()
             .statusCode(200)
             .body(containsString(emailTest));

    }

    @Test
    @DisplayName("Try to create User wrong email")
    public void postUser_whenAddUserWithWrongMail_ShouldReturnServerError() {

        UserApp user = UserApp.builder()
                .email("toto.com")
                .lastName("foo")
                .firstName("foo")
                .password("foofoofoo")
                .build();

        String jsonUser = JsonbBuilder.create().toJson(user);

        given()
                .when()
                .body(jsonUser).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_JSON)
                .post("/user")
                .then()
                .statusCode(500);

    }

    @Test
    @DisplayName("Search unknown user")
    public void getUser_whenSearchUnknownUserByEmail_ShouldReturn404() {

        String emailTest = "UNKNOWN@test.com";

        given()
                .when().param("email",emailTest).auth().basic("t.to","haha")
                .get("/user")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Add user")
    public void addUser_whenAddUser_ShouldReturn200() {

        Account account = new Account();
        account.bankName="New BANK corp.";
        account.iban="666";
        UserApp user = UserApp.builder()
                .email("foo@foo.com")
                .lastName("foo")
                .firstName("foo")
                .password("foofoofoo")
                .account(account)
                .build();

        String jsonUser = JsonbBuilder.create().toJson(user);

        given()
                .when()
                .body(jsonUser).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_JSON)
                .post("/user")
                .then()
                .statusCode(200);
    }

    @Test
    @DisplayName("Add friend")
    public void addFirend_whenAddAFriendToToto_ShouldReturn200() {
        Map<String,String> params = new HashMap<>();
        params.put("email_owner","toto@toto.com");
        params.put("email_user","tata@tata.com");

        given()
                .when()
                .formParams(params).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/user/friend")
                .then()
                .statusCode(200);
    }

    @Test
    @DisplayName("Add bank account")
    public void addAccount_whenAddHisBankAccount_ShouldReturn200() {

        Map<String,String> params = new HashMap<>();
        params.put("id","20");
        params.put("bank","MySuperBank !");
        params.put("iban","4534454AAA");

        given()
                .when()
                .formParams(params).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/user/21/account")
                .then()
                .statusCode(200);
    }

}