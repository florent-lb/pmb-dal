package com.pmb.test.integration.rest;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
@Tag("integration")
public class InternalTransactionEndPointTest {

    @Test
    @DisplayName("Transfer money from an account to another")
    public void transfer_whenTranserFromAnAcountToAnother_ShouldReturn200() {

        int idSender = 20,idReceiver = 21;
        double amount = 20.0 ;
        String description =  "For birthDay !";

        Map<String,String> params = new HashMap<>();
        params.put("id_sender", Integer.toString(idSender));
        params.put("id_receiver",Integer.toString(idReceiver));
        params.put("amount", Double.toString(amount));
        params.put("description",description);
        given()
                .when()
                .formParams(params).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/internal")
                .then()
                .body(
                        containsString("\"id\":"+idSender),
                        containsString("\"id\":"+idReceiver),
                        containsString("\"value\":"+amount),
                        containsString("\"description\":\""+description+"\"")
                )
                .statusCode(200);
    }

    @Test
    @DisplayName("Transfer money from an account to another with negative value")
    public void transfer_whenTranserFromAnAcountToAnotherWithNegativeValue_ShouldReturn500() {

        int idSender = 20,idReceiver = 21;
        double amount = -20.0 ;
        String description =  "For birthDay !";

        Map<String,String> params = new HashMap<>();
        params.put("id_sender", Integer.toString(idSender));
        params.put("id_receiver",Integer.toString(idReceiver));
        params.put("amount", Double.toString(amount));
        params.put("description",description);
        given()
                .when()
                .formParams(params).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/internal")
                .then()
                .statusCode(500);
    }

    @Test
    @DisplayName("Transfer money from an account to another with unknow user")
    public void transfer_whenTranserFromAnAcountToAnotherWithUnknowUser_ShouldReturn500() {

        int idSender = 500,idReceiver = 21;
        double amount = 20.0 ;
        String description =  "For birthDay !";

        Map<String,String> params = new HashMap<>();
        params.put("id_sender", Integer.toString(idSender));
        params.put("id_receiver",Integer.toString(idReceiver));
        params.put("amount", Double.toString(amount));
        params.put("description",description);
        given()
                .when()
                .formParams(params).auth().basic("t.to","haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/internal")
                .then()
                .statusCode(500);
    }

}