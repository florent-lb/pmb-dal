package com.pmb.test.integration.rest;

import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.enumeration.ExternalWay;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.internal.mapping.GsonMapper;
import io.restassured.mapper.ObjectMapper;
import io.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
@Tag("integration")
public class ExternalTransactionEndPointTest {

    @Test
    @DisplayName("Transfer money from bank account to app account")
    public void addMoney_whenAddMoneyOnUserAcount_ShouldReturn200() {

        String userId = "20";
        String valueT = "100";

        Map<String, String> params = new HashMap<>();
        params.put("id_user", userId);
        params.put("value", "100");

        given()
                .when()
                .formParams(params).auth().basic("t.to", "haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/external/to-app")
                .then()
                .body(
                        containsString("\"id\":" + userId),
                        containsString("\"value\":" + valueT),
                        containsString("\"label\":\"" + ExternalWay.FROM_BANK_ACCOUNT_TO_APP + "\"")
                )
                .statusCode(200);
    }

    @Test
    @DisplayName("Transfer money from app account to bank account")
    public void addMoney_whenAddMoneyOnUserBankAcount_ShouldReturn200() {

        String userId = "20";
        String valueT = "100";

        Map<String, String> params = new HashMap<>();
        params.put("id_user", userId);
        params.put("value", "100");

        given()
                .when()
                .formParams(params).auth().basic("t.to", "haha")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .post("/external/to-bank")
                .then()
                .body(
                        containsString("\"id\":" + userId),
                        containsString("\"value\":" + valueT),
                        containsString("\"label\":\"" + ExternalWay.FROM_APP_TO_BANK_ACCOUNT + "\"")
                )
                .statusCode(200);
    }

    @Test
    @DisplayName("Transfer money from app account to bank account")
    public void getTransactions_whenGetExternalTransactionFromBankToApp_ShouldReturn200WithList() {

        Integer userId = 20;

        Response resp = given()
                .when().auth().basic("t.to", "haha")
                .get("/external/" + userId + "/to-app").thenReturn();
        resp
                .then()
                .statusCode(200);
        List<ExternalTransaction> transactions = JsonbBuilder.create().fromJson(resp.getBody().print(),new ArrayList<ExternalTransaction>(){}.getClass().getGenericSuperclass());
        Assertions.assertThat(transactions)
                .isNotEmpty()
        .extracting(transaction -> transaction.pk.user.id).contains(userId);

    }

}