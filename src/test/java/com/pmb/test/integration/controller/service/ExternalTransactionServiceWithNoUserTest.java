package com.pmb.test.integration.controller.service;

import com.pmb.controller.service.contract.ExternalTransactionService;
import io.quarkus.arc.ArcUndeclaredThrowableException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.*;

@QuarkusTest
@Tag("integration")
public class ExternalTransactionServiceWithNoUserTest {

    @Inject
    ExternalTransactionService externalTransactionService;

    @Test
    @DisplayName("Transfert with wrong user")
    public void whenTransfert_withUserUnknowInBDD_ShouldReturnNullPointer() {
        Throwable throwable = catchThrowable(() -> externalTransactionService.transfertFromAppToBank(50, 10.0));

        assertThat(throwable)
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("No user found");

    }

    @Test
    public void whenTransfert_withNagtiveValue_ShouldReturnConstraintException() {
        Throwable throwable = catchThrowable(() -> externalTransactionService.transfertFromAppToBank(20, -10.0));

        assertThat(throwable)
                .isInstanceOf(ArcUndeclaredThrowableException.class);

    }
}
