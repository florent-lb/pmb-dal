package com.pmb.test.service;

import com.pmb.controller.service.TaxUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class TaxUtilsTest
{
    @ParameterizedTest
    @EnumSource(TaxUtils.Taxs.class)
    @DisplayName("Apply a Tax")
    public void calcTax_whenApplyATax_ShouldReturnTheCalculatedTax(TaxUtils.Taxs tax)
    {

        double basedValue = 100.0;
        Double valueAptemted = TaxUtils.calcTax(tax,basedValue);

        Assertions.assertThat(valueAptemted)
                .isEqualTo(basedValue*tax.getRationOnOne());


    }
}
