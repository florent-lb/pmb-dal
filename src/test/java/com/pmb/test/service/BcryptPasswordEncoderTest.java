package com.pmb.test.service;

import com.pmb.controller.security.impl.BcryptPasswordEncoder;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BcryptPasswordEncoderTest {
    private BcryptPasswordEncoder bcryptPasswordEncoder;

    @BeforeEach
    public void setup() {
        bcryptPasswordEncoder = new BcryptPasswordEncoder();
    }

    @Test
    @DisplayName("Security - Wrong Algorithm")
    public void encode_whenAlgoritmhIsNotFound_ShouldReturnEmptyPassword() {
        Assertions.assertThat(bcryptPasswordEncoder.encode("myPassword", "fakeAlgorithm"))
                .isEmpty();
    }
}
