-- References
INSERT INTO pmb.way VALUES (1,'FROM_APP_TO_BANK_ACCOUNT');
INSERT INTO pmb.way VALUES (2,'FROM_BANK_ACCOUNT_TO_APP');

INSERT INTO pmb.transaction_state VALUES (1,'BEGIN');
INSERT INTO pmb.transaction_state VALUES (10,'WAIT');
INSERT INTO pmb.transaction_state VALUES (20,'END');

-- Test/Dev DATA

INSERT INTO pmb.account (id,bank_name,iban) VALUES (20,'TOTO Corp.','54544343435');
INSERT INTO pmb.account (id,bank_name,iban) VALUES (25,'ATA Corp.','43643777');

INSERT INTO pmb.user_app (id, email, firstname, iteration, lastname, login_user,role_app, password, salt, wallet, id_account) VALUES (20, 'toto@toto.com', 'to', 6, 'to', 't.to','admin', '4qguIEezJJQGRW09x3BKCvjBpn1aqHg=', 'LAVjegWy54SKs7Y6RItsOg==', 20.23, 20);
INSERT INTO pmb.user_app (id, email, firstname, iteration, lastname, login_user,role_app, password, salt, wallet, id_account) VALUES (21, 'tata@tata.com', 'ta', 6, 'ta', 't.ta','user', '4qguIEezJJQGRW09x3BKCvjBpn1aqHg=', 'LAVjegWy54SKs7Y6RItsOg==', 200.1, 25);
UPDATE pmb.account SET id_user = 20 WHERE id = 20;
UPDATE pmb.account SET id_user = 21 WHERE id = 25;

INSERT INTO pmb.external_transaction (date_start, date_end, tax, value, id_user, id_state, id_way) VALUES ('2099-02-09 14:54:27.971000000', null, 0.50, 100.00, 20, 1, 2);