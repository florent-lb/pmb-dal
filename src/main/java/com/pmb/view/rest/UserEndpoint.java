package com.pmb.view.rest;

import com.pmb.controller.service.contract.ServiceFactory;
import com.pmb.model.dao.entity.UserApp;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.security.Authenticated;
import lombok.extern.log4j.Log4j2;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Log4j2
@RegisterForReflection
public class UserEndpoint {

    @Inject
    ServiceFactory serviceFactory;

    /**
     * Return A user of the app in JSON with his email
     * @param email email searched
     * @return 200 with user, 404 if not found or 500
     */
    @GET
    @Authenticated
    public Response getUser(@QueryParam("email") String email) {
        logger.info("REQUEST | GET user ->  params(email)" + email);
        try (Jsonb parser = JsonbBuilder.create()) {
            UserApp user = serviceFactory.getUserAppService().getUserByMail(email);
            return Response.ok(parser.toJson(user)).build();
        } catch (NullPointerException ex) {
            return Response.status(404, "No user found with email : " + email).build();
        } catch (Exception e) {
            logger.error("Impossible to parse Json Object ", e);
            return Response.serverError().build();
        }
    }

    /**
     * Add a user to app, reserved to admin account
     */
    @POST
    @RolesAllowed("admin")
    public Response addUser(UserApp user) {
        logger.info("REQUEST | POST user ->  params(user)" + user.toString());

        try {
            serviceFactory.getUserAppService().addUser(user);
            return Response.ok().build();
        } catch (ConstraintViolationException e) {
            logger.error("Impossible to add user", e);
            return Response.status(500, e.getMessage()).build();
        }
    }

    /**
     * Use for adding a User to friends user's list
     * @param emailOwner email of the user who want to add a friend
     * @param emailFriend email of the friend to add
     * @return 200 or 500
     */
    @POST
    @Path("/friend")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Authenticated
    public Response addFriend(@FormParam("email_owner") String emailOwner,
                              @FormParam("email_user") String emailFriend) {
        logger.info("REQUEST | POST user/friend ->  params(id_owner,id_user) : " + emailOwner + "," + emailFriend);

        serviceFactory.getUserAppService().addFriend(emailOwner, emailFriend);

        return Response.ok().build();
    }

    /**
     * Used for updating the bank account of the user in parameter
     * @param idUser id of the user
     * @param bankame New name of the bank
     * @param iban International Bank Account Number <a href="https://en.wikipedia.org/wiki/International_Bank_Account_Number">Wiki definition</a>
     * @return 200 or 500
     */
    @POST
    @Path("/{id}/account")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Authenticated
    public Response updateAccount(@PathParam("id") Integer idUser,
                                  @FormParam("bank") String bankame,
                                  @FormParam("iban") String iban) {
        logger.info("REQUEST | POST user/" + idUser + "/account ->  params(bank,iban) : " + bankame + "," + iban);

        serviceFactory.getUserAppService().updateAccount(idUser, bankame, iban);

        return Response.ok().build();
    }

}
