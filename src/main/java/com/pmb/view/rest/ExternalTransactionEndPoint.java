package com.pmb.view.rest;

import com.pmb.controller.service.contract.ServiceFactory;
import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.enumeration.ExternalWay;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.security.Authenticated;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Authenticated
@Path("external")
@RegisterForReflection
@Log4j2
public class ExternalTransactionEndPoint {

    @Inject
    ServiceFactory serviceFactory;

    /**
     * Used for transfer money from bank account to wallet app
     * @param idUser User who ask
     * @param value amount of money
     * @return The details of the transaction
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("to-app")
    public Response transferMoneyFromBankToApp(@FormParam("id_user") Integer idUser,
                                               @FormParam("value") Double value) {
        return Response.ok(serviceFactory.getExternalTransactionService()
                .transfertFromBankToApp(idUser, value)).build();
    }
    /**
     * Used for transfer money from wallet app to bank account
     * @param idUser User who ask
     * @param value amount of money
     * @return The details of the transaction
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("to-bank")
    public Response transferMoneyFromAppToBank(@FormParam("id_user") Integer idUser,
                                               @FormParam("value") Double value) {
        return Response.ok(serviceFactory.getExternalTransactionService()
                .transfertFromAppToBank(idUser, value)).build();
    }

    /**
     * Used for return all transaction from bank account to app for bills
     * @param idOwner User searched
     * @return List of the transaction {@link ExternalTransaction}
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/to-app")
    public Response getAllTransactionFromBankToAppAccount(@PathParam("id") Integer idOwner) {
        try (Jsonb jsonB = JsonbBuilder.create()) {
            List<ExternalTransaction> transactions = serviceFactory.getExternalTransactionService().getTransactionsOfUser(idOwner, ExternalWay.FROM_BANK_ACCOUNT_TO_APP);

            return Response.ok(jsonB.toJson(transactions)).build();
        } catch (Exception e) {
            logger.error("Impossible to return external transactions for user " + idOwner, e);
            return Response.serverError().build();
        }

    }
}
