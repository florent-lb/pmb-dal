package com.pmb.view.rest;

import com.pmb.controller.service.contract.ServiceFactory;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.security.Authenticated;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Authenticated
@Path("internal")
@RegisterForReflection
public class InternalTransactionEndPoint
{

    @Inject
    ServiceFactory serviceFactory;

    /**
     * Used for in-app transactions between two users
     * @param idUserSender ID UserApp of the user who send money
     * @param idReceiver ID UserApp of the user who receive it
     * @param amountToSend amount of the money to send
     * @param description Litteral description write by the user who send the money for the transaction
     * @return The details of the transaction
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfertMoneyFromUserToAnother(@FormParam("id_sender")Integer idUserSender,
                                                    @FormParam("id_receiver")Integer idReceiver,
                                                    @FormParam("amount")Double amountToSend, @FormParam("description")String description)
    {
        return Response.ok(serviceFactory.getInternalTransactionService()
                .createTransaction(idUserSender,idReceiver,amountToSend,description))
                .build();
    }

}
