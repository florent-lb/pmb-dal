package com.pmb.controller.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * Static utils class for calculate taxes
 */
@UtilityClass
public class TaxUtils {

    /**
     * Return the amount of the tax
     * @param taxToApply tax to apply see {@link Taxs}
     * @param value the value to apply tax ratio
     * @return
     */
    public double calcTax(Taxs taxToApply, double value)
    {
        return value * taxToApply.getRationOnOne();
    }

    /**
     * List of authorized taxes
     */
    @Getter
    @RequiredArgsConstructor
    public enum Taxs
    {
        TRANSACTION(0.005);

        /**
         * ratio for multiplier calc must be superior to 0 and not null
         */
        @Positive
        @NotNull
        private final double rationOnOne;
    }

}
