package com.pmb.controller.service.contract;


/**
 * Used fo Acces to services
 */
public interface ServiceFactory
{
    ExternalTransactionService getExternalTransactionService();

    InternalTransactionService getInternalTransactionService();

    UserAppService getUserAppService();
}
