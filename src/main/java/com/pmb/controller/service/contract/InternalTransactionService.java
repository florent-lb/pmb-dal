package com.pmb.controller.service.contract;

import com.pmb.model.dao.entity.InternalTransaction;

public interface InternalTransactionService {

    /**
     * Transactionnal Method for search user and create the transaction
     * @param idUserSender id of the sender
     * @param idReceiver id of the receiver
     * @param amountToSend amout of money to send, must be positive
     * @return The Transaction details
     */
    InternalTransaction createTransaction(Integer idUserSender, Integer idReceiver,Double amountToSend,String description);
}
