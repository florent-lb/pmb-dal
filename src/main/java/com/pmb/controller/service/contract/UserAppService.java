package com.pmb.controller.service.contract;

import com.pmb.model.dao.entity.UserApp;

import javax.validation.constraints.Email;


public interface UserAppService {
    /**
     * Search a User by his email
     * @param email a valid email {@link Email}
     * @return the user searched
     */
    UserApp getUserByMail(@Email String email);

    /**
     * Used for persist a new User
     * @param user the new user
     */
    void addUser(UserApp user);

    /**
     * Used for adding a new UserApp in a UserApp friends list
     * @param emailOwner valid email of the Owner of the list
     * @param emailToAdd valid email of the new user to add in the list
     */
    void addFriend(@Email String emailOwner, @Email String emailToAdd);

    /**
     * Used for update the bank account of a user
     * @param idUser id of the UserApp {@link UserApp#id}
     * @param bankName Name of the new Bank
     * @param iban Iban of the new Bank
     */
    void updateAccount(Integer idUser, String bankName, String iban);
}
