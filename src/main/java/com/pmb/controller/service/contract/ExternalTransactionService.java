package com.pmb.controller.service.contract;

import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.enumeration.ExternalWay;

import java.util.List;


public interface ExternalTransactionService
{
    /**
     * Transactionnal method for transfer money from Bank account to User App account
     * @param idUser id of appUser
     * @param value quantity of money to transfer
     * @return the transaction details
     */
    ExternalTransaction transfertFromBankToApp(Integer idUser, Double value);
    /**
     * Transactionnal method for transfer money from User app account to Bank account
     * @param idUser id of appUser
     * @param value quantity of money to transfer
     * @return the transaction details
     */
    ExternalTransaction transfertFromAppToBank(Integer idUser, Double value);

    List<ExternalTransaction> getTransactionsOfUser(Integer idUser, ExternalWay way);
}
