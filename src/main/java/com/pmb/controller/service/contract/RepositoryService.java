package com.pmb.controller.service.contract;

import com.pmb.model.dao.repository.contract.*;

/**
 * Used for acces to all repository
 */
public interface RepositoryService
{
    AccountRepository getAccountRepository();

    StateRepository getStateRepository();

    UserRepository getUserRepository();

    WayRepository getWayRepository();

    ExternalTransactionRepository getExternalTransactionRepository();
}
