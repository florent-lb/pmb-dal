package com.pmb.controller.service.impl;

import com.pmb.controller.security.contract.PasswordEncoder;
import com.pmb.controller.service.contract.RepositoryService;
import com.pmb.model.dao.entity.Account;
import com.pmb.model.dao.entity.UserApp;
import com.pmb.controller.service.contract.UserAppService;
import lombok.extern.log4j.Log4j2;
import org.wildfly.security.password.interfaces.BCryptPassword;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Base64;

@RequestScoped
@Log4j2
public class UserAppServiceImpl implements UserAppService {

    @Inject
    RepositoryService repositoryService;

    @Inject
    PasswordEncoder encoder;

    @Override
    public UserApp getUserByMail(String email) {
        return repositoryService.getUserRepository().findByEmail(email);
    }

    @Override
    @Transactional
    public void addUser(UserApp user) {
        //Password encoding
        BCryptPassword original = (BCryptPassword) encoder.encode(user.password).orElseThrow(() -> new NullPointerException("Impossible to generate password"));
        //Generate Login
        user.loginUser = generateLogin(user);
        user.password = Base64.getEncoder().encodeToString(original.getHash());
        user.salt = Base64.getEncoder().encodeToString(original.getSalt());
        user.iteration = (short) original.getIterationCount();
        user.id = null;
        user.roleApp = "user";
        user.wallet = 0.0;
        repositoryService.getUserRepository().persist(user);
    }


    private String generateLogin(UserApp user) {

        return Character.toString(user.firstName.charAt(0)) + '.' + user.lastName.trim();

    }

    /**
     * Transactional method for add a friend with id key
     *
     * @param emailOwner email of the user want add a friend
     * @param emailToAdd email of the friend
     */
    @Override
    @Transactional
    public void addFriend(String emailOwner, String emailToAdd) {
        UserApp owner = repositoryService.getUserRepository().findByEmail(emailOwner);
        UserApp friend = repositoryService.getUserRepository().findByEmail(emailToAdd);

        owner.getFriends().add(friend);

    }

    /**
     * Transactional Method for add an account to a user
     *
     * @param idUser   key of the user
     * @param bankName the name of the bank
     * @param iban     International Bank Account Number
     */
    @Override
    @Transactional
    public void updateAccount(Integer idUser, String bankName, String iban) {
        logger.debug("Add account to User : " + idUser);
        UserApp owner = repositoryService.getUserRepository().findById(idUser);
        Account account = new Account();
        account.iban = iban;
        account.bankName = bankName;
        account.setUserDetail(owner);
        account.persist();
        owner.account = account;
    }
}
