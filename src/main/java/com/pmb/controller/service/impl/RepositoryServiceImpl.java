package com.pmb.controller.service.impl;

import com.pmb.controller.service.contract.RepositoryService;
import com.pmb.model.dao.repository.contract.*;
import lombok.Getter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@Getter
@RequestScoped
class RepositoryServiceImpl implements RepositoryService {

    @Inject
    AccountRepository accountRepository;

    @Inject
    StateRepository stateRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    WayRepository wayRepository;

    @Inject
    ExternalTransactionRepository externalTransactionRepository;

}
