package com.pmb.controller.service.impl;

import com.pmb.controller.service.contract.ExternalTransactionService;
import com.pmb.controller.service.contract.InternalTransactionService;
import com.pmb.controller.service.contract.ServiceFactory;
import com.pmb.controller.service.contract.UserAppService;
import lombok.Getter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@Getter
@RequestScoped
public class ServiceFactoryImpl implements ServiceFactory {


    @Inject
    ExternalTransactionService externalTransactionService;
    @Inject
    InternalTransactionService internalTransactionService;
    @Inject
    UserAppService userAppService;

}
