package com.pmb.controller.service.impl;

import com.pmb.controller.service.TaxUtils;
import com.pmb.controller.service.contract.RepositoryService;
import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.dao.entity.ExternalTransactionPK;
import com.pmb.model.dao.entity.UserApp;
import com.pmb.model.enumeration.ExternalWay;
import com.pmb.controller.service.contract.ExternalTransactionService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static com.pmb.model.enumeration.ExternalWay.*;
import static com.pmb.model.enumeration.StateTag.*;


@RequestScoped
public class ExternalTransactionServiceImpl implements ExternalTransactionService {

    @Inject
    RepositoryService repositoryService;

    @Override
    @Transactional
    public ExternalTransaction transfertFromBankToApp(Integer idUser, Double value) {
        return transfert(idUser, value, FROM_BANK_ACCOUNT_TO_APP);
    }

    @Override
    @Transactional
    public ExternalTransaction transfertFromAppToBank(Integer idUser, Double value) {
        return transfert(idUser, value, FROM_APP_TO_BANK_ACCOUNT);
    }

    @Override
    public List<ExternalTransaction> getTransactionsOfUser(Integer idUser, ExternalWay way) {
        return repositoryService.getExternalTransactionRepository().getByUserId(idUser,way);
    }

    /**
     * Use for search the user and acquire the financial transaction
     *
     * @param idUser id of the user who ask the transaction
     * @param value  sum of money asked
     * @param way    the way of the transfer {@link ExternalWay}
     * @return The transaction Details
     * @throws NullPointerException if no User found, or other missings value {@link #acquireTransaction(UserApp, ExternalWay, Double)}
     */
    @Transactional
    private ExternalTransaction transfert(Integer idUser, Double value, ExternalWay way) {
        UserApp user = repositoryService.getUserRepository().findByIdOptional(idUser)
                .orElseThrow(() -> new NullPointerException("No user found with ID : " + idUser));
        return acquireTransaction(user, way, value);
    }

    /**
     * Method for initialize a transaction in BDD
     *
     * @param user  the user who ask the transaction
     * @param way   the way of the transfer {@link ExternalWay}
     * @param value sum of money asked
     * @return The transaction Details
     * @throws NullPointerException if user's account not completed, no state or way in BDD
     */
    @Transactional
    private ExternalTransaction acquireTransaction(UserApp user, ExternalWay way, Double value) {
        ExternalTransactionPK pk = new ExternalTransactionPK();
        pk.user = user;
        pk.dateStart = LocalDateTime.now();
        pk.state = repositoryService.getStateRepository().findByTag(BEGIN)
                .orElseThrow(() -> new NullPointerException("No state found : " + BEGIN.name()));
        ExternalTransaction transaction = new ExternalTransaction();
        transaction.pk = pk;
        transaction.value = value;
        transaction.tax = TaxUtils.calcTax(TaxUtils.Taxs.TRANSACTION, value);
        transaction.way = repositoryService.getWayRepository().findByLabel(way)
                .orElseThrow(() -> new NullPointerException("No way found : " + FROM_BANK_ACCOUNT_TO_APP.name()));
        transaction.persist();

        return transaction;
    }

}
