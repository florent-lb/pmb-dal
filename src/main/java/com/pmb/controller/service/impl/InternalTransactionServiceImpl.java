package com.pmb.controller.service.impl;

import com.pmb.controller.service.TaxUtils;
import com.pmb.controller.service.contract.InternalTransactionService;
import com.pmb.controller.service.contract.RepositoryService;
import com.pmb.model.dao.entity.InternalTransaction;
import com.pmb.model.dao.entity.UserApp;
import com.pmb.model.enumeration.StateTag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@RequestScoped
public class InternalTransactionServiceImpl implements InternalTransactionService {

    @Inject
    RepositoryService repositoryService;

    @Override
    @Transactional
    public InternalTransaction createTransaction(Integer idUserSender, Integer idReceiver, Double amountToSend, String description) {
        UserApp sender = repositoryService.getUserRepository().findByIdOptional(idUserSender).orElseThrow(() -> new NullPointerException("No sender found with id : " + idUserSender));
        UserApp receiver = repositoryService.getUserRepository().findByIdOptional(idReceiver).orElseThrow(() -> new NullPointerException("No receiver found with id : " + idReceiver));

        return acquireTransaction(sender, receiver, amountToSend, description);
    }

    @Transactional
    private InternalTransaction acquireTransaction(UserApp sender, UserApp receiver, Double amountToSend, String description) {
        InternalTransaction transaction = new InternalTransaction();
        transaction.dateStart = LocalDateTime.now();
        transaction.description = description;
        transaction.receiver = receiver;
        transaction.sender = sender;
        transaction.tax = TaxUtils.calcTax(TaxUtils.Taxs.TRANSACTION, amountToSend);
        transaction.state = repositoryService.getStateRepository().findByTag(StateTag.BEGIN)
                .orElseThrow(() -> new NullPointerException("No State found for : " + StateTag.BEGIN));
        transaction.value = amountToSend;
        transaction.persist();
        return transaction;
    }
}
