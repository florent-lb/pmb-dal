package com.pmb.controller.security.impl;

import lombok.extern.log4j.Log4j2;
import org.wildfly.security.password.Password;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;

import javax.enterprise.context.RequestScoped;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Optional;

/**
 * Bcrypt password implementation
 */
@Log4j2
@RequestScoped
public class BcryptPasswordEncoder implements com.pmb.controller.security.contract.PasswordEncoder {

    @Override
    public Optional<Password> encode(String passwordToEncode) {
        return encode(passwordToEncode,BCryptPassword.ALGORITHM_BCRYPT);
    }


    public Optional<Password> encode(String passwordToEncode,String algorithm) {
        try {
            PasswordFactory factory = PasswordFactory.getInstance(algorithm);
            SecureRandom rand = new SecureRandom();
            //Iteration between 5 & 10
            int iteration = rand.nextInt(BCryptPassword.DEFAULT_ITERATION_COUNT - 5) + 5;
            byte[] salt = new byte[BCryptPassword.BCRYPT_SALT_SIZE];
            rand.nextBytes(salt);

            IteratedSaltedPasswordAlgorithmSpec iteratedSaltedHashPasswordSpec = new IteratedSaltedPasswordAlgorithmSpec(iteration, salt);
            EncryptablePasswordSpec encryptablePasswordSpec = new EncryptablePasswordSpec(passwordToEncode.toCharArray(), iteratedSaltedHashPasswordSpec);

            BCryptPassword original = (BCryptPassword) factory.generatePassword(encryptablePasswordSpec);
            byte[] hash = original.getHash();

            Base64.Encoder encoder = Base64.getEncoder();
            logger.trace("Encoded SALT : " + encoder.encodeToString(salt));
            logger.trace("Encoded HASH : " + encoder.encodeToString(hash));
            return Optional.of(original);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
           logger.error("Impossible to generated password",e);
        }
        return Optional.empty();
    }



}
