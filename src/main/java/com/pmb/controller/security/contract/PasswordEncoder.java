package com.pmb.controller.security.contract;

import org.wildfly.security.password.Password;

import java.util.Optional;

public interface PasswordEncoder
{
    /**
     * Used for encode a clear String to a Password
     * Note : default implementation is Bcrypt
     * @param passwordToEncode clear password
     * @return Potentially a {@link Password}
     */
    Optional<Password> encode(String passwordToEncode);
}
