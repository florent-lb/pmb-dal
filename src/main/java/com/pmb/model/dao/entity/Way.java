package com.pmb.model.dao.entity;

import com.pmb.model.enumeration.ExternalWay;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Cacheable
@Table(name = "way")
public class Way extends PanacheEntityBase implements Serializable
{
    @Id
    public short id;

    @Enumerated(value = EnumType.STRING)
    @Column(length = 25)
    public ExternalWay label;
}
