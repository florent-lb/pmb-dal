package com.pmb.model.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExternalTransactionPK implements Serializable {

    @Column(name = "date_start")
    @PastOrPresent
    public LocalDateTime dateStart;

    @JoinColumn(name = "id_user",referencedColumnName = "id",foreignKey = @ForeignKey(name = "FK_user_id_external_transaction_id"))
    @ManyToOne(optional = false)
    public UserApp user;

    @JoinColumn(name = "id_state",referencedColumnName = "id",foreignKey = @ForeignKey(name = "FK_state_id_external_transaction_state"))
    @ManyToOne(optional = false)
    public State state;
}
