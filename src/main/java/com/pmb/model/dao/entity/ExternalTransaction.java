package com.pmb.model.dao.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "external_transaction")
@EqualsAndHashCode(of = {"pk"}, callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ExternalTransaction extends PanacheEntityBase implements Serializable {

    @EmbeddedId
    public ExternalTransactionPK pk;

    @Column(name = "value", columnDefinition = "DECIMAL(10,2)")
    @PositiveOrZero
    public Double value;

    @Column(name = "date_end")
    @FutureOrPresent
    public LocalDateTime dateEnd;

    @JoinColumn(name = "id_way", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_way_id_external_transaction_way"))
    @ManyToOne(optional = false)
    public Way way;

    @Column(name = "tax", columnDefinition = "DECIMAL(9,2)")
    @Positive
    public Double tax;
}
