package com.pmb.model.dao.entity;

import com.pmb.model.enumeration.StateTag;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "transaction_state")
@ToString
public class State extends PanacheEntityBase implements Serializable
{
    @Id
    @Column(name = "id")
    public Short id;

    @Column(name = "tag",length = 10)
    @Length(max = 10,min = 2)
    @Enumerated(value = EnumType.STRING)
    public StateTag tag;
}
