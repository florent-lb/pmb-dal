package com.pmb.model.dao.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "user_app",
        indexes = {
                @Index(name = "idx_email", columnList = "email")//Use for searching by email User
        })
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserApp extends PanacheEntityBase implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "email", unique = true)
    @Email
    public String email;

    @Column(name = "firstName")
    @NotBlank
    public String firstName;

    @Column(name = "lastName", nullable = false)
    @NotBlank
    public String lastName;

    @Column(name = "password",columnDefinition = "CHAR(32) NOT NULL")
    @NotBlank
    @Length(max = 255)
    public String password;

    @PositiveOrZero
    @Column(name = "wallet", precision = 2, scale = 20, nullable = false)
    public Double wallet;

    @Column(name = "login_user")
    @NotBlank
    public String loginUser;

    @Column(name = "salt", nullable = false,columnDefinition = "CHAR(24) NOT NULL")
    @JsonbTransient
    public String salt;

    @Column(name = "iteration")
    @Positive
    @JsonbTransient
    public short iteration;

    @Column(name = "role_app")
    @NotBlank
    public String roleApp;

    @Getter
    @Setter
    @ManyToMany
    @JsonbTransient
    @JoinTable(name = "user_list",
            joinColumns = {@JoinColumn(name = "id_owner", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_user_id_user_id_owner"))},
            inverseJoinColumns = {@JoinColumn(name = "id_user", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_user_id_user_id_user"))},
            indexes = {@Index(name = "idx_owner", columnList = "id_owner")})
    private List<UserApp> friends;

    @OneToOne(cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(name = "id_account", referencedColumnName = "id",foreignKey = @ForeignKey(name ="FK_account_id_user_id_account" ))
    public Account account;

}
