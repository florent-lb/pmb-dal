package com.pmb.model.dao.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.*;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account")
@ToString
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account extends PanacheEntityBase implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "bank_name",nullable = false)
    public String bankName;

    @Column(name = "iban",length = 34,nullable = false)
    public String iban;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user",referencedColumnName = "id",foreignKey = @ForeignKey(name = "FK_user_id_account_id_user"))
    @JsonbTransient
    private UserApp userDetail;


}
