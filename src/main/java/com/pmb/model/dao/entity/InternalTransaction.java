package com.pmb.model.dao.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "internal_transaction")
@EqualsAndHashCode(of = {"state", "sender", "receiver", "dateStart"}, callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InternalTransaction extends PanacheEntityBase implements Serializable {
    @ManyToOne
    @JoinColumn(name = "id_state", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_state_id_internal_transaction_state"))
    @Id
    public State state;

    @ManyToOne
    @JoinColumn(name = "id_sender", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_user_id_internal_transaction_sender"))
    @Id
    public UserApp sender;

    @ManyToOne
    @JoinColumn(name = "id_receiver", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_user_id_internal_transaction_receiver"))
    @Id
    public UserApp receiver;

    @Id
    @Column(name = "date_start")
    public LocalDateTime dateStart;

    @Column(name = "date_end")
    public LocalDateTime dateEnd;

    @Column(name = "value",columnDefinition = "DECIMAL(10,2)")
    @Positive
    public double value;

    @Column(name = "description")
    @Length(max = 1000)
    public String description;

    @Column(name = "tax",columnDefinition = "DECIMAL(9,2)")
    @Positive
    public Double tax;
}
