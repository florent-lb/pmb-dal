package com.pmb.model.dao.repository.contract;

import com.pmb.model.dao.entity.UserApp;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.validation.constraints.Email;

public interface UserRepository extends PanacheRepositoryBase<UserApp,Integer>
{
    /**
     * Searched an user by his email
     * @param email must be valid see {@link Email}
     * @return The user of NullPointer if not found
     */
    UserApp findByEmail(@Email String email);
}
