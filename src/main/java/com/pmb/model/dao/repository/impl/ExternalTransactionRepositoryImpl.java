package com.pmb.model.dao.repository.impl;

import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.dao.repository.contract.ExternalTransactionRepository;
import com.pmb.model.enumeration.ExternalWay;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@RequestScoped
public class ExternalTransactionRepositoryImpl implements ExternalTransactionRepository {

    @Inject
    EntityManager em;

    @Override
    public List<ExternalTransaction> getByUserId(Integer idUser) {

        return em.createQuery("SELECT t FROM ExternalTransaction t WHERE t.pk.user.id = :ID", ExternalTransaction.class)
                .setParameter("ID", idUser)
                .getResultList();
    }

    @Override
    public List<ExternalTransaction> getByUserId(Integer idUser, ExternalWay way) {

        return em.createQuery("SELECT t FROM ExternalTransaction t WHERE t.pk.user.id = :ID " +
                "AND t.way.label = :WAY", ExternalTransaction.class)
                .setParameter("ID", idUser)
                .setParameter("WAY", way)
                .getResultList();

    }
}
