package com.pmb.model.dao.repository.contract;

import com.pmb.model.dao.entity.ExternalTransaction;
import com.pmb.model.dao.entity.ExternalTransactionPK;
import com.pmb.model.enumeration.ExternalWay;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import java.util.List;

public interface ExternalTransactionRepository extends PanacheRepositoryBase<ExternalTransaction, ExternalTransactionPK>
{
    /**
     * Searched all Transaction of the user by his ID
     * @param idUser ID of UserApp
     * @return List of the transactions
     */
    List<ExternalTransaction> getByUserId(Integer idUser);

    /**
     * See {@link #getByUserId(Integer)}
     * @param way the way of the transactions searched
     */
    List<ExternalTransaction> getByUserId(Integer idUser, ExternalWay way);
}
