package com.pmb.model.dao.repository.contract;

import com.pmb.model.dao.entity.Way;
import com.pmb.model.enumeration.ExternalWay;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import java.util.Optional;

public interface WayRepository extends PanacheRepositoryBase<Way, Short> {
    /**
     * Search a Way based on the enumeration in parameter
     *
     * @param way {@link com.pmb.model.enumeration.ExternalWay}
     * @return Potentially the entity Way search
     */
    Optional<Way> findByLabel(ExternalWay way);
}
