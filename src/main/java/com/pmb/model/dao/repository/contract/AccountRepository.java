package com.pmb.model.dao.repository.contract;

import com.pmb.model.dao.entity.Account;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

public interface AccountRepository extends PanacheRepositoryBase<Account,Integer> {

}
