package com.pmb.model.dao.repository.impl;

import com.pmb.model.dao.entity.State;
import com.pmb.model.dao.repository.contract.StateRepository;
import com.pmb.model.enumeration.StateTag;

import javax.enterprise.context.RequestScoped;
import java.util.Optional;

@RequestScoped
public class StateRepositoryImpl implements StateRepository {

    @Override
    public Optional<State> findByTag(StateTag tag)
    {
        return find("tag",tag).firstResultOptional();
    }


}
