package com.pmb.model.dao.repository.contract;

import com.pmb.model.dao.entity.State;
import com.pmb.model.enumeration.StateTag;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import java.util.Optional;

public interface StateRepository extends PanacheRepositoryBase<State,Short> {
    /**
     * Used for found a State by its tag
     * @param tag See {@link StateTag}
     * @return Potentially the State searched
     */
    Optional<State> findByTag(StateTag tag);
}
