package com.pmb.model.dao.repository.impl;

import com.pmb.model.dao.entity.Way;
import com.pmb.model.dao.repository.contract.WayRepository;
import com.pmb.model.enumeration.ExternalWay;

import javax.enterprise.context.RequestScoped;
import java.util.Optional;

@RequestScoped
public class WayRepositoryImpl implements WayRepository {

    @Override
    public Optional<Way> findByLabel(ExternalWay label)
    {
        return find("label",label).firstResultOptional();
    }


}
