package com.pmb.model.dao.repository.impl;

import com.pmb.model.dao.entity.UserApp;
import com.pmb.model.dao.repository.contract.UserRepository;
import lombok.extern.log4j.Log4j2;

import javax.enterprise.context.RequestScoped;

@RequestScoped
@Log4j2
public class UserRepositoryImpl implements UserRepository {

    @Override
    public UserApp findByEmail(String email)
    {
        return find("email",email).firstResultOptional().orElseThrow(() -> new NullPointerException("No User found with email : " + email));
    }

}
