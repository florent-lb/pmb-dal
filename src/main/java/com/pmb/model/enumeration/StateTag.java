package com.pmb.model.enumeration;

/**
 * Used for references in table pmb.transaction_state
 * Tags indicate the state of a transaction
 */
public enum StateTag
{
    BEGIN,
    WAITING,
    END
}
