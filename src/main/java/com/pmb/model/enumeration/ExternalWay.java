package com.pmb.model.enumeration;

/**
 * Enumeration used for references in table pmb.way
 * Indicate the way of an external transaction between bank account and wallet app
 */
public enum  ExternalWay
{
    FROM_APP_TO_BANK_ACCOUNT,
    FROM_BANK_ACCOUNT_TO_APP;
}
